# bi0s blog
blog.bi0s.in proudly published by [teambi0s](https://bi0s.in)

## Building locally

To set up and work with this project locally, follow the steps below:

1. Fork, clone or download this project
2. [Install Hexo](https://hexo.io/docs/)
3. Install all dependencies with `npm install`
4. To add the writeups:
    + Add content to `source/_posts/<category>/` in markdown
    + Or type `hexo new post <title>` to create with post_asset_folder
      + Note that executing the above command will create a post in `source/_posts/` directory. Hence, move it to an appropriate category directory: `source/_posts/<category>/`
5. Run the preview locally: `hexo server`
    + Use `-p <port>` to run on a different port than default `4000`
6. Send over a pull request!

Read more at [Hexo docs](https://hexo.io/docs/)

##  Features Implemented
* Author name to post layout
* Supports 2 Authors, and Author Webpage link
  + If more than two people have contributed to a write-up, inform us in the group and we will make necessary changes
* Google-Analytics
* Post Encrypt
* PWA
* Local Search
* Slide HTML <alpha>
* Disqus Comment
* Local Search

## Ongoing Tasks
* Post breadcrumb
* Google CSE

## To-Do
* GDrive Sync
* AMP
* Custom Fields

## General guidelines:
* Always use `<!--more-->` to split the post with excerpt
  + See https://gitlab.com/teambi0s/blog/raw/master/source/_posts/Crypto/Digital-Signatures/midnightsun-quals19-ezdsa.md for reference
* Use footnotes with [^1]
    * More Syntax [here](https://github.com/LouisBarranqueiro/hexo-footnotes)
* Slide HTML in front-matter, (use any one of the folowing)
    * slidehtml: true
    * slidehtml:
        titleMerge: true
        verticalSeparator: \n--\n
    * slidehtml:
        titleMerge: true
* Never encrypt your blog
* Use author2 and author2-url for 2nd author (in the front-matter)
  + For example:
    + author2: spyd3r
    + author2-url: https://twitter.com/TarunkantG
* To add latex equations to your post
  + Enable it in your post by adding `mathjax: true` in the [front-matter](https://hexo.io/docs/front-matter) of your post
  + You can now add latex equations by enclosing them in `$`. For example: `$ a + b = c $`

## Still have queries?
Feel free to approach Ashutosh or Sayooj!

## Sample Front-Matter
```front-matter
---
title: Pretty

Complete this!!!
```
